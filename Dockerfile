FROM java:8
MAINTAINER Justin Bleach <justinbleach@gmail.com>

#### Install node(and npm) and gulp-cli ####
ENV NPM_CONFIG_LOGLEVEL info
ENV NODE_VERSION 4.4.3

RUN apt-get -q -y update

# not concerned with checking SHA etc
RUN curl -SLO "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz" \
&& tar -xJf "node-v${NODE_VERSION}-linux-x64.tar.xz" -C /usr/local --strip-components=1 \
&& rm "node-v${NODE_VERSION}-linux-x64.tar.xz"

RUN npm -v 
RUN npm install -g gulp-cli
RUN apt-get clean && rm -r /var/lib/apt/lists/*    

#### Install gradle ####
ENV GRADLE_VERSION 2.9
ENV GRADLE_HOME /usr/bin/gradle
ENV GRADLE_USER_HOME /cache

ADD https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip /usr/bin/gradle.zip
WORKDIR /usr/bin
RUN apt-get install -y unzip && \
    unzip gradle.zip && \
    ln -s gradle-${GRADLE_VERSION} gradle && \
    rm gradle.zip      

# clean up
RUN apt-get remove -y unzip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*      

ENV PATH $PATH:$GRADLE_HOME/bin

VOLUME $GRADLE_USER_HOME
VOLUME /app

WORKDIR /app
CMD ["gradle", "-version"]

## No entry point so can be used with continuous build